//
//  BezierCurve.c
//  frizby
//
//  Created by Gromov on 21.01.16.
//  Copyright © 2016 Gromov. All rights reserved.
//

#include "BezierCurve.h"
#include <stdio.h>
//#include <malloc.h>
#include <math.h>

#define MAXPOINTS	1000

///Enter point

static	void		FitCubic();
static	double *    Reparameterize();
static	double		NewtonRaphsonRootFind();
static	CGPoint		BezierII();
static	double 		B0(), B1(), B2(), B3();
static	CGPoint		ComputeLeftTangent();
static	CGPoint		ComputeRightTangent();
static	CGPoint		ComputeCenterTangent();
static	double		ComputeMaxError();
static	double *    ChordLengthParameterize();
static	BezierCurve	GenerateBezier();
static	CGPoint		V2AddII();
static	CGPoint		V2ScaleIII();
static	CGPoint		V2SubII();

void FitCurve(CGPoint	*d, int	nPts, double error)
//CGPoint	*d;			/*  Array of digitized points	*/
//int		nPts;		/*  Number of digitized points	*/
//double	error;		/*  User-defined error squared	*/
{
    CGPoint	tHat1, tHat2;	/*  Unit tangent vectors at endpoints */
    
    tHat1 = ComputeLeftTangent(d, 0);
    tHat2 = ComputeRightTangent(d, nPts - 1);
    FitCubic(d, 0, nPts - 1, tHat1, tHat2, error);
}

void DrawBezierCurve(int n, BezierCurve curve)
{
    /* You'll have to write this yourself. */
}

static double V2DistanceBetween2Points(CGPoint *a, CGPoint *b)
{
    double dx = (*a).x - (*b).x;
    double dy = (*a).y - (*b).y;
    return (sqrt((dx * dx) + (dy * dy)));
}

static double V2SquaredLength(CGPoint *a)
{
    return (((*a).x * (*a).x) + ((*a).y * (*a).y));
}

static double V2Length(CGPoint *a)
{
    return (sqrt(V2SquaredLength(a)));
}

static CGPoint *V2Scale(CGPoint *v, double newlen)
{
    double len = V2Length(v);
    if (len != 0.0) {
        (*v).x *= newlen / len;
        (*v).y *= newlen / len;
    }
    return v;
}

static CGPoint *V2Add(CGPoint *a, CGPoint *b, CGPoint *c)
{
    (*c).x = (*a).x + (*b).x;
    (*c).y = (*a).y + (*b).y;
    return c;
}

static CGPoint * V2Negate(CGPoint *v)
{
    (*v).x = -(*v).x;
    (*v).y = -(*v).y;
    return v;
}

static double V2Dot(CGPoint *a, CGPoint *b)
{
    return (((*a).x * (*b).x) + ((*a).y * (*b).y));
}

static CGPoint * V2Normalize(CGPoint * v)
{
    double len = V2Length(v);
    if (len != 0.0) {
        (*v).x /= len;
        (*v).y /= len;
    }
    return v;
}


/*
 *  FitCubic :
 *  	Fit a Bezier curve to a (sub)set of digitized points
 */
static void FitCubic(CGPoint *d, int first, int last, CGPoint tHat1, CGPoint tHat2, double error)
//CGPoint	*d;			/*  Array of digitized points */
//int		first, last;	/* Indices of first and last pts in region */
//CGPoint	tHat1, tHat2;	/* Unit tangent vectors at endpoints */
//double	error;		/*  User-defined error squared	   */
{
    BezierCurve	bezCurve; /*Control points of fitted Bezier curve*/
    double	*u;		/*  Parameter values for point  */
    double	*uPrime;	/*  Improved parameter values */
    double	maxError;	/*  Maximum fitting error	 */
    int		splitPoint;	/*  Point to split point set at	 */
    int		nPts;		/*  Number of points in subset  */
    double	iterationError; /*Error below which you try iterating  */
    int		maxIterations = 4; /*  Max times to try iterating  */
    CGPoint	tHatCenter;   	/* Unit tangent vector at splitPoint */
    int		i;
    
    iterationError = error * error;
    nPts = last - first + 1;
    
    /*  Use heuristic if region only has two points in it */
    if (nPts == 2) {
        double dist = V2DistanceBetween2Points(&d[last], &d[first]) / 3.0;
        
        bezCurve = (CGPoint *)malloc(4 * sizeof(CGPoint));
        bezCurve[0] = d[first];
        bezCurve[3] = d[last];
        V2Add(&bezCurve[0], V2Scale(&tHat1, dist), &bezCurve[1]);
        V2Add(&bezCurve[3], V2Scale(&tHat2, dist), &bezCurve[2]);
        DrawBezierCurve(3, bezCurve);
        free((void *)bezCurve);
        return;
    }
    
    /*  Parameterize points, and attempt to fit curve */
    u = ChordLengthParameterize(d, first, last);
    bezCurve = GenerateBezier(d, first, last, u, tHat1, tHat2);
    
    /*  Find max deviation of points to fitted curve */
    maxError = ComputeMaxError(d, first, last, bezCurve, u, &splitPoint);
    if (maxError < error) {
        DrawBezierCurve(3, bezCurve);
        free((void *)u);
        free((void *)bezCurve);
        return;
    }
    
    
    /*  If error not too large, try some reparameterization  */
    /*  and iteration */
    if (maxError < iterationError) {
        for (i = 0; i < maxIterations; i++) {
            uPrime = Reparameterize(d, first, last, u, bezCurve);
            bezCurve = GenerateBezier(d, first, last, uPrime, tHat1, tHat2);
            maxError = ComputeMaxError(d, first, last,
                                       bezCurve, uPrime, &splitPoint);
            if (maxError < error) {
                DrawBezierCurve(3, bezCurve);
                free((void *)u);
                free((void *)bezCurve);
                return;
            }
            free((void *)u);
            u = uPrime;
        }
    }
    
    /* Fitting failed -- split at max error point and fit recursively */
    free((void *)u);
    free((void *)bezCurve);
    tHatCenter = ComputeCenterTangent(d, splitPoint);
    FitCubic(d, first, splitPoint, tHat1, tHatCenter, error);
    V2Negate(&tHatCenter);
    FitCubic(d, splitPoint, last, tHatCenter, tHat2, error);
}


/*
 *  GenerateBezier :
 *  Use least-squares method to find Bezier control points for region.
 *
 */
static BezierCurve  GenerateBezier(CGPoint * d, int first, int last, double *uPrime, CGPoint tHat1, CGPoint tHat2)
//CGPoint	*d;			/*  Array of digitized points	*/
//int		first, last;		/*  Indices defining region	*/
//double	*uPrime;		/*  Parameter values for region */
//CGPoint	tHat1, tHat2;	/*  Unit tangents at endpoints	*/
{
    int 	i;
    CGPoint 	A[MAXPOINTS][2];	/* Precomputed rhs for eqn	*/
    int 	nPts;			/* Number of pts in sub-curve */
    double 	C[2][2];			/* Matrix C		*/
    double 	X[2];			/* Matrix X			*/
    double 	det_C0_C1,		/* Determinants of matrices	*/
    det_C0_X,
    det_X_C1;
    double 	alpha_l,		/* Alpha values, left and right	*/
    alpha_r;
    CGPoint 	tmp;			/* Utility variable		*/
    BezierCurve	bezCurve;	/* RETURN bezier curve ctl pts	*/
    
    bezCurve = (CGPoint *)malloc(4 * sizeof(CGPoint));
    nPts = last - first + 1;
    
    
    /* Compute the A's	*/
    for (i = 0; i < nPts; i++) {
        CGPoint		v1, v2;
        v1 = tHat1;
        v2 = tHat2;
        V2Scale(&v1, B1(uPrime[i]));
        V2Scale(&v2, B2(uPrime[i]));
        A[i][0] = v1;
        A[i][1] = v2;
    }
    
    /* Create the C and X matrices	*/
    C[0][0] = 0.0;
    C[0][1] = 0.0;
    C[1][0] = 0.0;
    C[1][1] = 0.0;
    X[0]    = 0.0;
    X[1]    = 0.0;
    
    for (i = 0; i < nPts; i++) {
        C[0][0] += V2Dot(&A[i][0], &A[i][0]);
        C[0][1] += V2Dot(&A[i][0], &A[i][1]);
        /*					C[1][0] += V2Dot(&A[i][0], &A[i][1]);*/
        C[1][0] = C[0][1];
        C[1][1] += V2Dot(&A[i][1], &A[i][1]);
        
        tmp = V2SubII(d[first + i],
                      V2AddII(
                              V2ScaleIII(d[first], B0(uPrime[i])),
                              V2AddII(
                                      V2ScaleIII(d[first], B1(uPrime[i])),
                                      V2AddII(
                                              V2ScaleIII(d[last], B2(uPrime[i])),
                                              V2ScaleIII(d[last], B3(uPrime[i]))))));
        
        
        X[0] += V2Dot(&A[i][0], &tmp);
        X[1] += V2Dot(&A[i][1], &tmp);
    }
    
    /* Compute the determinants of C and X	*/
    det_C0_C1 = C[0][0] * C[1][1] - C[1][0] * C[0][1];
    det_C0_X  = C[0][0] * X[1]    - C[0][1] * X[0];
    det_X_C1  = X[0]    * C[1][1] - X[1]    * C[0][1];
    
    /* Finally, derive alpha values	*/
    if (det_C0_C1 == 0.0) {
        det_C0_C1 = (C[0][0] * C[1][1]) * 10e-12;
    }
    alpha_l = det_X_C1 / det_C0_C1;
    alpha_r = det_C0_X / det_C0_C1;
    
    
    /*  If alpha negative, use the Wu/Barsky heuristic (see text) */
    /* (if alpha is 0, you get coincident control points that lead to
     * divide by zero in any subsequent NewtonRaphsonRootFind() call. */
    if (alpha_l < 1.0e-6 || alpha_r < 1.0e-6) {
        double	dist = V2DistanceBetween2Points(&d[last], &d[first]) /
        3.0;
        
        bezCurve[0] = d[first];
        bezCurve[3] = d[last];
        V2Add(&bezCurve[0], V2Scale(&tHat1, dist), &bezCurve[1]);
        V2Add(&bezCurve[3], V2Scale(&tHat2, dist), &bezCurve[2]);
        return (bezCurve);
    }
    
    /*  First and last control points of the Bezier curve are */
    /*  positioned exactly at the first and last data points */
    /*  Control points 1 and 2 are positioned an alpha distance out */
    /*  on the tangent vectors, left and right, respectively */
    bezCurve[0] = d[first];
    bezCurve[3] = d[last];
    V2Add(&bezCurve[0], V2Scale(&tHat1, alpha_l), &bezCurve[1]);
    V2Add(&bezCurve[3], V2Scale(&tHat2, alpha_r), &bezCurve[2]);
    return (bezCurve);
}


/*
 *  Reparameterize:
 *	Given set of points and their parameterization, try to find
 *   a better parameterization.
 *
 */
static double *Reparameterize(CGPoint * d, int first, int last, double * u, BezierCurve bezCurve)
//CGPoint	*d;			/*  Array of digitized points	*/
//int		first, last;		/*  Indices defining region	*/
//double	*u;			/*  Current parameter values	*/
//BezierCurve	bezCurve;	/*  Current fitted curve	*/
{
    int 	nPts = last-first+1;
    int 	i;
    double	*uPrime;		/*  New parameter values	*/
    
    uPrime = (double *)malloc(nPts * sizeof(double));
    for (i = first; i <= last; i++) {
        uPrime[i-first] = NewtonRaphsonRootFind(bezCurve, d[i], u[i-
                                                                  first]);
    }
    return (uPrime);
}



/*
 *  NewtonRaphsonRootFind :
 *	Use Newton-Raphson iteration to find better root.
 */
static double NewtonRaphsonRootFind(BezierCurve Q, CGPoint P, double u)
//BezierCurve	Q;			/*  Current fitted curve	*/
//CGPoint 		P;		/*  Digitized point		*/
//double 		u;		/*  Parameter value for "P"	*/
{
    double 		numerator, denominator;
    CGPoint 		Q1[3], Q2[2];	/*  Q' and Q''			*/
    CGPoint		Q_u, Q1_u, Q2_u; /*u evaluated at Q, Q', & Q''	*/
    double 		uPrime;		/*  Improved u			*/
    int 		i;
    
    /* Compute Q(u)	*/
    Q_u = BezierII(3, Q, u);
    
    /* Generate control vertices for Q'	*/
    for (i = 0; i <= 2; i++) {
        Q1[i].x = (Q[i+1].x - Q[i].x) * 3.0;
        Q1[i].y = (Q[i+1].y - Q[i].y) * 3.0;
    }
    
    /* Generate control vertices for Q'' */
    for (i = 0; i <= 1; i++) {
        Q2[i].x = (Q1[i+1].x - Q1[i].x) * 2.0;
        Q2[i].y = (Q1[i+1].y - Q1[i].y) * 2.0;
    }
    
    /* Compute Q'(u) and Q''(u)	*/
    Q1_u = BezierII(2, Q1, u);
    Q2_u = BezierII(1, Q2, u);
    
    /* Compute f(u)/f'(u) */
    numerator = (Q_u.x - P.x) * (Q1_u.x) + (Q_u.y - P.y) * (Q1_u.y);
    denominator = (Q1_u.x) * (Q1_u.x) + (Q1_u.y) * (Q1_u.y) +
    (Q_u.x - P.x) * (Q2_u.x) + (Q_u.y - P.y) * (Q2_u.y);
    
    /* u = u - f(u)/f'(u) */
    uPrime = u - (numerator/denominator);
    return (uPrime);
}



/*
 *  Bezier :
 *  	Evaluate a Bezier curve at a particular parameter value
 *
 */
static CGPoint BezierII(int degree, CGPoint * V, double t)
//int		degree;		/* The degree of the bezier curve	*/
//CGPoint 	*V;		/* Array of control points		*/
//double 	t;		/* Parametric value to find point for	*/
{
    int 	i, j;
    CGPoint 	Q;	        /* Point on curve at parameter t	*/
    CGPoint 	*Vtemp;		/* Local copy of control points		*/
    
    /* Copy array	*/
    Vtemp = (CGPoint *)malloc((unsigned)((degree+1)
                                        * sizeof (CGPoint)));
    for (i = 0; i <= degree; i++) {
        Vtemp[i] = V[i];
    }
    
    /* Triangle computation	*/
    for (i = 1; i <= degree; i++) {
        for (j = 0; j <= degree-i; j++) {
            Vtemp[j].x = (1.0 - t) * Vtemp[j].x + t * Vtemp[j+1].x;
            Vtemp[j].y = (1.0 - t) * Vtemp[j].y + t * Vtemp[j+1].y;
        }
    }
    
    Q = Vtemp[0];
    free((void *)Vtemp);
    return Q;
}


/*
 *  B0, B1, B2, B3 :
 *	Bezier multipliers
 */
static double B0(double u)
//double	u;
{
    double tmp = 1.0 - u;
    return (tmp * tmp * tmp);
}


static double B1(double u)
//double	u;
{
    double tmp = 1.0 - u;
    return (3 * u * (tmp * tmp));
}

static double B2(double u)
//double	u;
{
    double tmp = 1.0 - u;
    return (3 * u * u * tmp);
}

static double B3(double u)
//double	u;
{
    return (u * u * u);
}



/*
 * ComputeLeftTangent, ComputeRightTangent, ComputeCenterTangent :
 *Approximate unit tangents at endpoints and "center" of digitized curve
 */
static CGPoint ComputeLeftTangent(CGPoint * d, int end)
//CGPoint	*d;			/*  Digitized points*/
//int		end;		/*  Index to "left" end of region */
{
    CGPoint	tHat1;
    tHat1 = V2SubII(d[end+1], d[end]);
    tHat1 = *V2Normalize(&tHat1);
    return tHat1;
}

static CGPoint ComputeRightTangent(CGPoint * d, int end)
//CGPoint	*d;			/*  Digitized points		*/
//int		end;		/*  Index to "right" end of region */
{
    CGPoint	tHat2;
    tHat2 = V2SubII(d[end-1], d[end]);
    tHat2 = *V2Normalize(&tHat2);
    return tHat2;
}


static CGPoint ComputeCenterTangent(CGPoint	* d, int center)
//CGPoint	*d;			/*  Digitized points			*/
//int		center;		/*  Index to point inside region	*/
{
    CGPoint	V1, V2, tHatCenter;
    
    V1 = V2SubII(d[center-1], d[center]);
    V2 = V2SubII(d[center], d[center+1]);
    tHatCenter.x = (V1.x + V2.x)/2.0;
    tHatCenter.y = (V1.y + V2.y)/2.0;
    tHatCenter = *V2Normalize(&tHatCenter);
    return tHatCenter;
}


/*
 *  ChordLengthParameterize :
 *	Assign parameter values to digitized points
 *	using relative distances between points.
 */
static double *ChordLengthParameterize(CGPoint * d, int first, int last)
//CGPoint	*d;			/* Array of digitized points */
//int		first, last;		/*  Indices defining region	*/
{
    int		i;
    double	*u;			/*  Parameterization		*/
    
    u = (double *)malloc((unsigned)(last-first+1) * sizeof(double));
    
    u[0] = 0.0;
    for (i = first+1; i <= last; i++) {
        u[i-first] = u[i-first-1] +
        V2DistanceBetween2Points(&d[i], &d[i-1]);
    }
    
    for (i = first + 1; i <= last; i++) {
        u[i-first] = u[i-first] / u[last-first];
    }
    
    return(u);
}




/*
 *  ComputeMaxError :
 *	Find the maximum squared distance of digitized points
 *	to fitted curve.
 */
static double ComputeMaxError(CGPoint * d, int first, int last, BezierCurve bezCurve, double * u, int * splitPoint)
//CGPoint	*d;			/*  Array of digitized points	*/
//int		first, last;		/*  Indices defining region	*/
//BezierCurve	bezCurve;		/*  Fitted Bezier curve		*/
//double	*u;			/*  Parameterization of points	*/
//int		*splitPoint;		/*  Point of maximum error	*/
{
    int		i;
    double	maxDist;		/*  Maximum error		*/
    double	dist;		/*  Current error		*/
    CGPoint	P;			/*  Point on curve		*/
    CGPoint	v;			/*  Vector from point to curve	*/
    
    *splitPoint = (last - first + 1)/2;
    maxDist = 0.0;
    for (i = first + 1; i < last; i++) {
        P = BezierII(3, bezCurve, u[i-first]);
        v = V2SubII(P, d[i]);
        dist = V2SquaredLength(&v);
        if (dist >= maxDist) {
            maxDist = dist;
            *splitPoint = i;
        }
    }
    return (maxDist);
}
static CGPoint V2AddII(CGPoint a, CGPoint b)
//CGPoint a, b;
{
    CGPoint	c;
    c.x = a.x + b.x;  c.y = a.y + b.y;
    return (c);
}
static CGPoint V2ScaleIII(CGPoint v, double s)
//CGPoint	v;
//double	s;
{
    CGPoint result;
    result.x = v.x * s; result.y = v.y * s;
    return (result);
}

static CGPoint V2SubII(CGPoint a, CGPoint b)
//CGPoint	a, b;
{
    CGPoint	c;
    c.x = a.x - b.x; c.y = a.y - b.y;
    return (c);
}