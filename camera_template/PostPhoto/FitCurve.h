//
//  FitCurve.h
//  frizby
//
//  Created by Gromov on 21.01.16.
//  Copyright © 2016 Gromov. All rights reserved.
//

#ifndef FitCurve_h
#define FitCurve_h

#include <stdio.h>
#include <vector>
#include <math.h>

typedef std::vector<float> vectorF;

class TriDiagonalMatrixF
{
public:
    vectorF A;
    
    /// <summary>
    /// The values for the main diagonal.
    /// </summary>
    vectorF B;
    
    /// <summary>
    /// The values for the super-diagonal. C[C.Length-1] is never used.
    /// </summary>
    vectorF C;
    
    /// <summary>
    /// The width and height of this matrix.
    /// </summary>
    int N;
    
    /// <summary>
    /// Indexer. Setter throws an exception if you try to set any not on the super, main, or sub diagonals.
    /// </summary>
    float get(int row, int col);
    void  set(int row, int col, float value);
    
    ~TriDiagonalMatrixF();
    /// <summary>
    /// Construct an NxN matrix.
    /// </summary>
    TriDiagonalMatrixF(int n):A(n), B(n), C(n)
    {
        N = n;
    }
    
    /// <summary>
    /// Produce a string representation of the contents of this matrix.
    /// </summary>
    /// <param name="fmt">Optional. For String.Format. Must include the colon. Examples are ':0.000' and ',5:0.00' </param>
    /// <param name="prefix">Optional. Per-line indentation prefix.</param>
//    std::string ToDisplayString(std::string fmt = "", std::string prefix = "");
    
    /// <summary>
    /// Solve the system of equations this*x=d given the specified d.
    /// </summary>
    /// <remarks>
    /// Uses the Thomas algorithm described in the wikipedia article: http://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm
    /// Not optimized. Not destructive.
    /// </remarks>
    /// <param name="d">Right side of the equation.</param>
    vectorF Solve(float d []);
};

#endif /* FitCurve_h */
