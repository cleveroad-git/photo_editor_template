//
//  CubicSpline.h
//  frizby
//
//  Created by Gromov on 27.01.16.
//  Copyright © 2016 Gromov. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "FitCurve.h"

class CubicSpline
{
private:
    vectorF a;
    vectorF b;
    
    // Save the original x and y for Eval
    vectorF xOrig;
    vectorF yOrig;
    
    int _lastIndex;
    
    void CheckAlreadyFitted();
    int GetNextXIndex(float x);
    float EvalSpline(float x, int j, bool debug = false);
    
public:
    
    CubicSpline()
    {
        _lastIndex = 0;
    }
    
    /// <summary>
    /// Construct and call Fit.
    /// </summary>
    /// <param name="x">Input. X coordinates to fit.</param>
    /// <param name="y">Input. Y coordinates to fit.</param>
    /// <param name="startSlope">Optional slope constraint for the first point. Single.NaN means no constraint.</param>
    /// <param name="endSlope">Optional slope constraint for the final point. Single.NaN means no constraint.</param>
    /// <param name="debug">Turn on console output. Default is false.</param>
    CubicSpline(vectorF x, vectorF y, float startSlope = -1, float endSlope = -1, bool debug = false)
    {
        _lastIndex = 0;
        Fit(x, y, startSlope, endSlope, debug);
    }
    
    /// <summary>
    /// Fit x,y and then eval at points xs and return the corresponding y's.
    /// This does the "natural spline" style for ends.
    /// This can extrapolate off the ends of the splines.
    /// You must provide points in X sort order.
    /// </summary>
    /// <param name="x">Input. X coordinates to fit.</param>
    /// <param name="y">Input. Y coordinates to fit.</param>
    /// <param name="xs">Input. X coordinates to evaluate the fitted curve at.</param>
    /// <param name="startSlope">Optional slope constraint for the first point. Single.NaN means no constraint.</param>
    /// <param name="endSlope">Optional slope constraint for the final point. Single.NaN means no constraint.</param>
    /// <param name="debug">Turn on console output. Default is false.</param>
    /// <returns>The computed y values for each xs.</returns>
    vectorF FitAndEval(vectorF x, vectorF y, vectorF xs, float startSlope = -1, float endSlope = -1, bool debug = false);
    
    /// <summary>
    /// Compute spline coefficients for the specified x,y points.
    /// This does the "natural spline" style for ends.
    /// This can extrapolate off the ends of the splines.
    /// You must provide points in X sort order.
    /// </summary>
    /// <param name="x">Input. X coordinates to fit.</param>
    /// <param name="y">Input. Y coordinates to fit.</param>
    /// <param name="startSlope">Optional slope constraint for the first point. Single.NaN means no constraint.</param>
    /// <param name="endSlope">Optional slope constraint for the final point. Single.NaN means no constraint.</param>
    /// <param name="debug">Turn on console output. Default is false.</param>
    void Fit(vectorF x, vectorF y, float startSlope = -1, float endSlope = -1, bool debug = false);
    
    /// <summary>
    /// Evaluate the spline at the specified x coordinates.
    /// This can extrapolate off the ends of the splines.
    /// You must provide X's in ascending order.
    /// The spline must already be computed before calling this, meaning you must have already called Fit() or FitAndEval().
    /// </summary>
    /// <param name="x">Input. X coordinates to evaluate the fitted curve at.</param>
    /// <param name="debug">Turn on console output. Default is false.</param>
    /// <returns>The computed y values for each x.</returns>
    vectorF Eval(vectorF x, bool debug = false);
    
    /// <summary>
    /// Evaluate (compute) the slope of the spline at the specified x coordinates.
    /// This can extrapolate off the ends of the splines.
    /// You must provide X's in ascending order.
    /// The spline must already be computed before calling this, meaning you must have already called Fit() or FitAndEval().
    /// </summary>
    /// <param name="x">Input. X coordinates to evaluate the fitted curve at.</param>
    /// <param name="debug">Turn on console output. Default is false.</param>
    /// <returns>The computed y values for each x.</returns>
    vectorF EvalSlope(vectorF x, bool debug = false);
    
    /// <summary>
    /// Static all-in-one method to fit the splines and evaluate at X coordinates.
    /// </summary>
    /// <param name="x">Input. X coordinates to fit.</param>
    /// <param name="y">Input. Y coordinates to fit.</param>
    /// <param name="xs">Input. X coordinates to evaluate the fitted curve at.</param>
    /// <param name="startSlope">Optional slope constraint for the first point. Single.NaN means no constraint.</param>
    /// <param name="endSlope">Optional slope constraint for the final point. Single.NaN means no constraint.</param>
    /// <param name="debug">Turn on console output. Default is false.</param>
    /// <returns>The computed y values for each xs.</returns>
    static vectorF Compute(vectorF x, vectorF y, vectorF xs, float startSlope = -1, float endSlope = -1, bool debug = false);
    
    /// <summary>
    /// Fit the input x,y points using a 'geometric' strategy so that y does not have to be a single-valued
    /// function of x.
    /// </summary>
    /// <param name="x">Input x coordinates.</param>
    /// <param name="y">Input y coordinates, do not need to be a single-valued function of x.</param>
    /// <param name="nOutputPoints">How many output points to create.</param>
    /// <param name="xs">Output (interpolated) x values.</param>
    /// <param name="ys">Output (interpolated) y values.</param>
    static void FitGeometric(vectorF x, vectorF y, int nOutputPoints, vectorF & xs, vectorF & ys);
    

};
