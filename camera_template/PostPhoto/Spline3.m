//
//  Spline3.m
//  frizby
//
//  Created by Gromov on 25.01.16.
//  Copyright © 2016 Gromov. All rights reserved.
//

#import "Spline3.h"
#import "DrawPoint.h"

@interface Spline3 () {
    NSArray * inputArray;
    NSInteger N;
    float *x, *y, *h, *l, *delta, *lambda, *c, *d, *b;
}

@end

@implementation Spline3

- (void)dealloc
{
    [self freematrix];
}

- (instancetype)initWithArray:(NSArray *)points
{
    self = [super init];
    if (self) {
        inputArray = points;
        N = inputArray.count - 1;
        [self allocmatrix];
        [self fillData];
    }
    return self;
}

- (void)allocmatrix
{
    //allocate memory for matrixes
    x = malloc(sizeof(float) * (N+1));
    y = malloc(sizeof(float) * (N+1));
    h = malloc(sizeof(float) * (N+1));
    l = malloc(sizeof(float) * (N+1));
    delta = malloc(sizeof(float) * (N+1));
    lambda = malloc(sizeof(float) * (N+1));
    c = malloc(sizeof(float) * (N+1));
    d = malloc(sizeof(float) * (N+1));
    b = malloc(sizeof(float) * (N+1));
}

- (void)fillData
{
    for (int i = 0; i <= N; ++i) {
        DrawPoint * point = inputArray[i];
        x[i] = point.point.x;
        y[i] = point.point.y;
    }
}

- (void)performInterpolation
{
    NSInteger k=0;
    for(k=1; k<=N; k++){
        h[k] = x[k] - x[k-1];
        if(h[k]==0){
            printf("\nError, x[%ld]=x[%ld]\n", (long)k, k-1);
            return;
        }
        l[k] = (y[k] - y[k-1])/h[k];
    }
    delta[1] = - h[2]/(2*(h[1]+h[2]));
    lambda[1] = 1.5*(l[2] - l[1])/(h[1]+h[2]);
    for(k=3; k<=N; k++){
        delta[k-1] = - h[k]/(2*h[k-1] + 2*h[k] + h[k-1]*delta[k-2]);
        lambda[k-1] = (3*l[k] - 3*l[k-1] - h[k-1]*lambda[k-2]) /
        (2*h[k-1] + 2*h[k] + h[k-1]*delta[k-2]);
    }
    c[0] = 0;
    c[N] = 0;
    for(k = N; k >= 2; k--){
        c[k-1] = delta[k-1]*c[k] + lambda[k-1];
    }
    for(k = 1; k <= N; k++){
        d[k] = (c[k] - c[k-1])/(3*h[k]);
        b[k] = l[k] + (2*c[k]*h[k] + h[k]*c[k-1])/3;
    }
}

- (NSArray *)getOutputArray
{
    NSMutableArray * output = [NSMutableArray arrayWithCapacity:N*3+1];
    for (int i = 0; i < N; ++i) {
        DrawPoint * current = inputArray[i];
        output[i*3] = inputArray[i];
        float dx = (x[i+1] - x[i])/3;
        float F1 = y[i] + b[i]*dx + c[i]*pow(dx, 2) + d[i]*pow(dx, 3);
        output[i*3 + 1] = [current copyWithPoint:CGPointMake(x[i] + dx, F1)];
        
        dx *= 2;
        float F2 = y[i] + b[i]*dx + c[i]*pow(dx, 2) + d[i]*pow(dx, 3);
        output[i*3 + 2] = [current copyWithPoint:CGPointMake(x[i] + dx, F2)];
    }
    [output addObject:[inputArray lastObject]];
    return output;
//    float start = x[0];
//    float end = x[N];
//    float step = (end - start)/(N*3);
//    FILE* OutFile = fopen("test.txt", "wt");
//    for(float s = start; s<=end; s+= step){
//        //find k, where s in [x_k-1; x_k]
//        for(int k=1; k<=N; k++){
//            if(s>=x[k-1] && s<=x[k]){
//                break;
//            }
//        }
//        float F = y[k] + b[k]*(s-x[k]) + c[k]*pow(s-x[k], 2) + d[k]*pow(s-x[k], 3);
//        fprintf(OutFile, "%f\t%f\n", s,  F);
//    }
//    fclose(OutFile);
}

- (void)freematrix
{
    free(x);
    free(y);
    free(h);
    free(l);
    free(delta);
    free(lambda);
    free(c);
    free(d);
    free(b);
}

@end
