//
//  FitCurveNSO.m
//  frizby
//
//  Created by Gromov on 21.01.16.
//  Copyright © 2016 Gromov. All rights reserved.
//

#import "FitCurveNSO.h"
#include <vector>
#import "CubicSpline.h"
#import "DrawPoint.h"

@implementation FitCurveNSO

- (NSArray *)getFitCurveFromArray:(NSArray *)array
{
    NSUInteger size = array.count;
    vectorF x(size), y(size), xs(size*3), ys(size*3);
    DrawPoint * dp = nil;
    for (int i = 0; i < size; ++i)
    {
        dp = array[i];
        x[i] = dp.point.x;
        y[i] = dp.point.y;
    }
    CubicSpline::FitGeometric(x, y, (int)size*3, xs, ys);
    NSMutableArray * res = [NSMutableArray arrayWithCapacity:xs.size()];
    for (int i = 0; i < xs.size(); ++i)
    {
        if (xs[i] != NAN && ys[i] != NAN) {
            [res addObject:[dp copyWithPoint:CGPointMake(xs[i], ys[i])]];
        }
    }

    
    return (res.count > size*2) ? res : [array copy];
}

@end
