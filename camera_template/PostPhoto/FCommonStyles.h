//
//  FCommonStyles.h
//  Frizby
//
//  Created by Gromov on 30.09.15.
//  Copyright © 2015 Gromov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FCommonStyles : NSObject

+ (UIColor *)mainThemeColour;
+ (UIColor *)blackColor;
+ (UIColor *)whiteColor;
+ (UIColor *)redColor;
+ (UIColor *)orangeColor;
+ (UIColor *)yellowColor;
+ (UIColor *)azureColor;
+ (UIColor *)blueColor;
+ (UIColor *)greenColor;
+ (UIColor *)red2Color;
+ (UIColor *)grayColor;
+ (NSArray *)getColors;
+ (UIColor *)pinkColor;
+ (UIColor *)leagueTextColor;
+ (BOOL)isRedColor:(UIColor *)color;

@end
