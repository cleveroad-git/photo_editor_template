//
//  BezierCurve.h
//  frizby
//
//  Created by Gromov on 21.01.16.
//  Copyright © 2016 Gromov. All rights reserved.
//

#ifndef BezierCurve_h
#define BezierCurve_h

#include <stdio.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


typedef CGPoint * BezierCurve;

/* Forward declarations */
void FitCurve();

#endif /* BezierCurve_h */
