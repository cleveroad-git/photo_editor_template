//
//  FitCurve.c
//  frizby
//
//  Created by Gromov on 21.01.16.
//  Copyright © 2016 Gromov. All rights reserved.
//

#include "FitCurve.h"

#include <cstdlib> // for malloc and free
#include <stdio.h>
#include <math.h>

using namespace std;

float TriDiagonalMatrixF::get(int row, int col)
{
    int di = row - col;
    
    if (di == 0)
    {
        return B[row];
    }
    else if (di == -1)
    {
//        Debug.Assert(row < N - 1);
        return C[row];
    }
    else if (di == 1)
    {
//        Debug.Assert(row > 0);
        return A[row];
    }
    else return 0;
}

void TriDiagonalMatrixF::set(int row, int col, float value)
{
    int di = row - col;
    
    if (di == 0)
    {
        B[row] = value;
    }
    else if (di == -1)
    {
//        Debug.Assert(row < N - 1);
        C[row] = value;
    }
    else if (di == 1)
    {
//        Debug.Assert(row > 0);
        A[row] = value;
    }
    else
    {
//        error
    }
}

TriDiagonalMatrixF::~TriDiagonalMatrixF()
{
//    delete [] A;
//    delete [] B;
//    delete [] C;
}
/// <summary>
/// Construct an NxN matrix.
/// </summary>
//TriDiagonalMatrixF::TriDiagonalMatrixF(int n)
//{
//    this->A = new float[n];
//    this->B = new float[n];
//    this->C = new float[n];
//}

/// <summary>
/// Produce a string representation of the contents of this matrix.
/// </summary>
/// <param name="fmt">Optional. For String.Format. Must include the colon. Examples are ':0.000' and ',5:0.00' </param>
/// <param name="prefix">Optional. Per-line indentation prefix.</param>
//    std::string ToDisplayString(std::string fmt = "", std::string prefix = "");

/// <summary>
/// Solve the system of equations this*x=d given the specified d.
/// </summary>
/// <remarks>
/// Uses the Thomas algorithm described in the wikipedia article: http://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm
/// Not optimized. Not destructive.
/// </remarks>
/// <param name="d">Right side of the equation.</param>
vectorF TriDiagonalMatrixF::Solve(float d [])
{
    int n = N;
    
    // cPrime
    float * cPrime = new float[n];
    cPrime[0] = C[0] / B[0];
    
    for (int i = 1; i < n; i++)
    {
        cPrime[i] = C[i] / (B[i] - cPrime[i-1] * A[i]);
    }
    
    // dPrime
    float * dPrime = new float[n];
    dPrime[0] = d[0] / B[0];
    
    for (int i = 1; i < n; i++)
    {
        dPrime[i] = (d[i] - dPrime[i-1]*A[i]) / (B[i] - cPrime[i - 1] * A[i]);
    }
    
    // Back substitution
    vectorF x(n);
    x[n - 1] = dPrime[n - 1];
    
    for (int i = n-2; i >= 0; i--)
    {
        x[i] = dPrime[i] - cPrime[i] * x[i + 1];
    }
    
    delete [] cPrime;
    delete [] dPrime;
    
    return x;
}