//
//  FitCurveNSO.h
//  frizby
//
//  Created by Gromov on 21.01.16.
//  Copyright © 2016 Gromov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FitCurveNSO : NSObject

- (NSArray *)getFitCurveFromArray:(NSArray *)array;

@end
