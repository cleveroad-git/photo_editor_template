//
//  DrawPoint.m
//  frizby
//
//  Created by Gromov on 27.11.15.
//  Copyright © 2015 Gromov. All rights reserved.
//

#import "DrawPoint.h"

@implementation DrawPoint

- (DrawPoint *)copyWithPoint:(CGPoint)point
{
    DrawPoint * newDP = [[DrawPoint alloc] init];
    newDP.point = point;
    newDP.red = self.red;
    newDP.green = self.green;
    newDP.blue = self.blue;
    newDP.brush = self.brush;
    return newDP;
}

@end
