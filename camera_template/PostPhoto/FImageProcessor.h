//
//  FImageProcessor.h
//  Frizby
//
//  Created by Gromov on 30.09.15.
//  Copyright © 2015 Gromov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class DrawPoint;

@interface FImageProcessor : NSObject

+ (UIImage *)imageFromView:(UIView *)view;
+ (UIImage *)drawImage:(UIImage *)image intoImage:(UIImage *)baseimage inRect:(CGRect)rect;
+ (CGRect)calculateClientRectOfImageInUIImageView:(UIImageView *)imgView;
+ (CGRect)multiplyRect:(CGRect)rect multiplier:(float)multiplier;
+ (NSData *)getCompressedDataForImage:(UIImage *)image;
+ (UIImage *)compressImageIfNeed:(UIImage *)image;
+ (UIImage *)cropImage:(UIImage *)image withAspectRatio:(CGFloat)ratio;
+ (UIImage *)drawLines:(NSArray<NSArray<DrawPoint *> *> *)lines toImage:(UIImage *)image;

@end
