//
//  Spline3.h
//  frizby
//
//  Created by Gromov on 25.01.16.
//  Copyright © 2016 Gromov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Spline3 : NSObject

- (instancetype)initWithArray:(NSArray *)points;

- (void)performInterpolation;
- (NSArray *)getOutputArray;

@end
