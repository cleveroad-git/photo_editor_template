//
//  CubicSpline.m
//  frizby
//
//  Created by Gromov on 27.01.16.
//  Copyright © 2016 Gromov. All rights reserved.
//

#import "CubicSpline.h"

void CubicSpline::CheckAlreadyFitted()
{
    if (a.size() == 0) {
        NSException * ex = [NSException exceptionWithName:@"Fit must be called before you can evaluate." reason:@"Fit must be called before you can evaluate." userInfo:nil];
        [ex raise];
    }
}

int CubicSpline::GetNextXIndex(float x)
{
    if (x < xOrig[_lastIndex])
    {
        NSException * ex = [NSException exceptionWithName:@"ArgumentExeption" reason:@"The X values to evaluate must be sorted." userInfo:nil];
        [ex raise];
    }
    
    while ((_lastIndex < xOrig.size() - 2) && (x > xOrig[_lastIndex + 1]))
    {
        _lastIndex++;
    }
    
    return _lastIndex;
}

float CubicSpline::EvalSpline(float x, int j, bool debug)
{
    float dx = xOrig[j + 1] - xOrig[j];
    float t = (x - xOrig[j]) / dx;
    float y = (1 - t) * yOrig[j] + t * yOrig[j + 1] + t * (1 - t) * (a[j] * (1 - t) + b[j] * t); // equation 9
//    if (debug) Console.WriteLine("xs = {0}, j = {1}, t = {2}", x, j, t);
    return y;
}

vectorF CubicSpline::FitAndEval(vectorF x, vectorF y, vectorF xs, float startSlope, float endSlope, bool debug)
{
    Fit(x, y, startSlope, endSlope, debug);
    return Eval(xs, debug);
}


void CubicSpline::Fit(vectorF x, vectorF y, float startSlope, float endSlope, bool debug)
{
    // Save x and y for eval
    xOrig = x;
    yOrig = y;
    
    int n = x.size();
    float * r = new float[n]; // the right hand side numbers: wikipedia page overloads b
    
    TriDiagonalMatrixF m = TriDiagonalMatrixF(n);
    float dx1, dx2, dy1, dy2;
    
    // First row is different (equation 16 from the article)
    if (startSlope == -1)
    {
        dx1 = x[1] - x[0];
        m.C[0] = 1.0f / dx1;
        m.B[0] = 2.0f * m.C[0];
        r[0] = 3 * (y[1] - y[0]) / (dx1 * dx1);
    }
    else
    {
        m.B[0] = 1;
        r[0] = startSlope;
    }
    
    // Body rows (equation 15 from the article)
    for (int i = 1; i < n - 1; i++)
    {
        dx1 = x[i] - x[i - 1];
        dx2 = x[i + 1] - x[i];
        
        m.A[i] = 1.0f / dx1;
        m.C[i] = 1.0f / dx2;
        m.B[i] = 2.0f * (m.A[i] + m.C[i]);
        
        dy1 = y[i] - y[i - 1];
        dy2 = y[i + 1] - y[i];
        r[i] = 3 * (dy1 / (dx1 * dx1) + dy2 / (dx2 * dx2));
    }
    
    // Last row also different (equation 17 from the article)
    if (endSlope == -1)
    {
        dx1 = x[n - 1] - x[n - 2];
        dy1 = y[n - 1] - y[n - 2];
        m.A[n - 1] = 1.0f / dx1;
        m.B[n - 1] = 2.0f * m.A[n - 1];
        r[n - 1] = 3 * (dy1 / (dx1 * dx1));
    }
    else
    {
        m.B[n - 1] = 1;
        r[n - 1] = endSlope;
    }
    
//    if (debug) Console.WriteLine("Tri-diagonal matrix:\n{0}", m.ToDisplayString(":0.0000", "  "));
//    if (debug) Console.WriteLine("r: {0}", ArrayUtil.ToString<float>(r));
    
    // k is the solution to the matrix
    vectorF k = m.Solve(r);
//    if (debug) Console.WriteLine("k = {0}", ArrayUtil.ToString<float>(k));
    
    // a and b are each spline's coefficients
    a = vectorF(n-1);
    b = vectorF(n-1);
    
    for (int i = 1; i < n; i++)
    {
        dx1 = x[i] - x[i - 1];
        dy1 = y[i] - y[i - 1];
        a[i - 1] = k[i - 1] * dx1 - dy1; // equation 10 from the article
        b[i - 1] = -k[i] * dx1 + dy1; // equation 11 from the article
    }
    
//    if (debug) Console.WriteLine("a: {0}", ArrayUtil.ToString<float>(a));
//    if (debug) Console.WriteLine("b: {0}", ArrayUtil.ToString<float>(b));
}


vectorF CubicSpline::Eval(vectorF x, bool debug)
{
    CheckAlreadyFitted();
    
    NSUInteger n = x.size();
    vectorF y(n);
    _lastIndex = 0; // Reset simultaneous traversal in case there are multiple calls
    
    for (int i = 0; i < n; i++)
    {
        // Find which spline can be used to compute this x (by simultaneous traverse)
        int j = GetNextXIndex(x[i]);
        
        // Evaluate using j'th spline
        y[i] = EvalSpline(x[i], j, debug);
    }
    
    return y;
}


vectorF CubicSpline::EvalSlope(vectorF x, bool debug)
{
    CheckAlreadyFitted();
    
    NSUInteger n = x.size();
    vectorF qPrime(n);
    _lastIndex = 0; // Reset simultaneous traversal in case there are multiple calls
    
    for (int i = 0; i < n; i++)
    {
        // Find which spline can be used to compute this x (by simultaneous traverse)
        int j = GetNextXIndex(x[i]);
        
        // Evaluate using j'th spline
        float dx = xOrig[j + 1] - xOrig[j];
        float dy = yOrig[j + 1] - yOrig[j];
        float t = (x[i] - xOrig[j]) / dx;
        
        // From equation 5 we could also compute q' (qp) which is the slope at this x
        qPrime[i] = dy / dx
        + (1 - 2 * t) * (a[j] * (1 - t) + b[j] * t) / dx
        + t * (1 - t) * (b[j] - a[j]) / dx;
        
//        if (debug) Console.WriteLine("[{0}]: xs = {1}, j = {2}, t = {3}", i, x[i], j, t);
    }
    
    return qPrime;
}


vectorF CubicSpline::Compute(vectorF x, vectorF y, vectorF xs, float startSlope, float endSlope, bool debug)
{
    CubicSpline spline;
    return spline.FitAndEval(x, y, xs, startSlope, endSlope, debug);
}


void CubicSpline::FitGeometric(vectorF x, vectorF y, int nOutputPoints, vectorF & xs, vectorF & ys)
{
    // Compute distances
    NSUInteger n = x.size();
    vectorF dists(n); // cumulative distance
    dists[0] = 0;
    float totalDist = 0;
    
    for (int i = 1; i < n; i++)
    {
        float dx = x[i] - x[i - 1];
        float dy = y[i] - y[i - 1];
        float dist = (float)sqrt(dx * dx + dy * dy);
        totalDist += dist;
        dists[i] = totalDist;
    }
    
    // Create 'times' to interpolate to
    float dt = totalDist / (nOutputPoints - 1);
    vectorF times(nOutputPoints);
    times[0] = 0;
    
    for (int i = 1; i < nOutputPoints; i++)
    {
        times[i] = times[i - 1] + dt;
    }
    
    // Spline fit both x and y to times
    CubicSpline xSpline;
    vectorF xst = xSpline.FitAndEval(dists, x, times);
    xs.swap(xst);
    
    CubicSpline ySpline;
    vectorF yst = ySpline.FitAndEval(dists, y, times);
    ys.swap(yst);
}


