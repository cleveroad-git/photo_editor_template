//
//  PPColorButton.h
//  frizby
//
//  Created by Gromov on 19.10.15.
//  Copyright © 2015 Gromov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PPColorButton : UIButton {
    UIColor * backgroundColor;
}

- (void)makeCircleFormWithColor:(UIColor *)color;
- (void)setBorder;
- (void)removeBorder;

@end
