//
//  FCommonStyles.m
//  Frizby
//
//  Created by Gromov on 30.09.15.
//  Copyright © 2015 Gromov. All rights reserved.
//

#import "FCommonStyles.h"

@implementation FCommonStyles

+ (UIColor *)mainThemeColour
{
    static UIColor * mainThemeColour = nil;
    if (!mainThemeColour){
        mainThemeColour = [UIColor colorWithRed:253/255.0 green:91/255.0 blue:100/255.0 alpha:1.0];
    }
    return mainThemeColour;
}

+ (UIColor *)blackColor
{
    return [UIColor blackColor];
}

+ (UIColor *)whiteColor
{
    return [UIColor whiteColor];
}

+ (UIColor *)redColor;
{
    static UIColor * redColor = nil;
    if (!redColor){
        redColor = [UIColor redColor];
    }
    return redColor;
}

+ (UIColor *)orangeColor
{
    return [UIColor orangeColor];
}

+ (UIColor *)yellowColor
{
    return [UIColor yellowColor];
}

+ (UIColor *)azureColor
{
    return [UIColor colorWithRed:0/255.0 green:127/255.0 blue:255/255.0 alpha:1.0];
}

+ (UIColor *)blueColor
{
    return [UIColor blueColor];
}

+ (UIColor *)greenColor
{
    return [UIColor greenColor];
}

+ (UIColor *)red2Color
{
    static UIColor * red2Color = nil;
    if (!red2Color){
        red2Color = [UIColor colorWithRed:227/255.0 green:38/255.0 blue:54/255.0 alpha:1.0];
    }
    return red2Color;
}

+ (UIColor *)pinkColor
{
    static UIColor * pinkColor = nil;
    if (!pinkColor){
        pinkColor = [UIColor colorWithRed:255/255.0 green:203/255.0 blue:219/255.0 alpha:1.0];
    }
    return pinkColor;
}

+ (UIColor *)grayColor
{
    return [UIColor darkGrayColor];
}

+ (NSArray *)getColors
{
    return @[[FCommonStyles blackColor], [FCommonStyles whiteColor], [FCommonStyles redColor], [FCommonStyles orangeColor], [FCommonStyles yellowColor], [FCommonStyles azureColor], [FCommonStyles blueColor], [FCommonStyles greenColor], [FCommonStyles red2Color], [FCommonStyles grayColor], [FCommonStyles whiteColor], [FCommonStyles whiteColor]];
}

+ (UIColor *)leagueTextColor
{
    static UIColor * leagueTextColor = nil;
    if (!leagueTextColor) {
        leagueTextColor = [UIColor colorWithRed:42/255.0 green:35/255.0 blue:36/255.0 alpha:1.0];
    }
    return leagueTextColor;
}

+ (BOOL)isRedColor:(UIColor *)color
{
    return [color isEqual:[FCommonStyles redColor]] || [color isEqual:[FCommonStyles red2Color]];
}

@end
