//
//  FImageProcessor.m
//  Frizby
//
//  Created by Gromov on 30.09.15.
//  Copyright © 2015 Gromov. All rights reserved.
//

#import "FImageProcessor.h"
#import "DrawPoint.h"

@implementation FImageProcessor

+ (UIImage *)imageFromView:(UIView *)view
{
    UIGraphicsBeginImageContext(view.bounds.size);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (UIImage *)drawImage:(UIImage *)image intoImage:(UIImage *)baseimage inRect:(CGRect)rect
{
    if (!image) {
        return baseimage;
    }
    CGRect imageRect = {CGPointZero,baseimage.size};
    NSInteger inputWidth = CGRectGetWidth(imageRect);
    NSInteger inputHeight = CGRectGetHeight(imageRect);
    
    // 1) Calculate the location of Ghosty
    UIImage * ghostImage = image;
//    CGFloat ghostImageAspectRatio = rect.size.width / rect.size.height;
    
//    NSInteger targetGhostWidth = inputWidth * 0.25;
//    CGSize ghostSize = CGSizeMake(targetGhostWidth, targetGhostWidth / ghostImageAspectRatio);
//    CGPoint ghostOrigin = CGPointMake(inputWidth * 0.5, inputHeight * 0.2);
    
    CGRect ghostRect = rect;
    
    // 2) Draw your image into the context.
    UIGraphicsBeginImageContext(baseimage.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGAffineTransform flip = CGAffineTransformMakeScale(1.0, -1.0);
    CGAffineTransform flipThenShift = CGAffineTransformTranslate(flip,0,-inputHeight);
    CGContextConcatCTM(context, flipThenShift);
    
    CGContextDrawImage(context, imageRect, [baseimage CGImage]);
    
    CGContextSetBlendMode(context, kCGBlendModeSourceAtop);
    CGContextSetAlpha(context,1.0);
    CGRect transformedGhostRect = CGRectApplyAffineTransform(ghostRect, flipThenShift);
    CGContextDrawImage(context, transformedGhostRect, [ghostImage CGImage]);
    
    // 3) Retrieve your processed image
    UIImage * imageWithGhost = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageWithGhost;
}

+ (CGRect)calculateClientRectOfImageInUIImageView:(UIImageView *)imgView
{
    CGSize imgViewSize=imgView.frame.size;                  // Size of UIImageView
    CGSize imgSize=imgView.image.size;                      // Size of the image, currently displayed
    
    // Calculate the aspect, assuming imgView.contentMode==UIViewContentModeScaleAspectFit
    
    CGFloat scaleW = imgViewSize.width / imgSize.width;
    CGFloat scaleH = imgViewSize.height / imgSize.height;
    CGFloat aspect=fmin(scaleW, scaleH);
    
    CGRect imageRect = { {0,0} , { imgSize.width*aspect, imgSize.height*aspect } };
    
    // Note: the above is the same as :
    // CGRect imageRect=CGRectMake(0,0,imgSize.width*=aspect,imgSize.height*=aspect) I just like this notation better
    
    // Center image
    
    imageRect.origin.x=(imgViewSize.width-imageRect.size.width)/2;
    imageRect.origin.y=(imgViewSize.height-imageRect.size.height)/2;
    
    // Add imageView offset
    
//    imageRect.origin.x+=imgView.frame.origin.x;
//    imageRect.origin.y+=imgView.frame.origin.y;
    
    return(imageRect);
}

+ (CGRect)multiplyRect:(CGRect)rect multiplier:(float)multiplier
{
    return CGRectMake(CGRectGetMinX(rect)*multiplier, CGRectGetMinY(rect)*multiplier, CGRectGetWidth(rect)*multiplier, CGRectGetHeight(rect)*multiplier);
}

+ (NSData *)getCompressedDataForImage:(UIImage *)image
{
    float compressionQuality = 0.9;
    float minCompression = 0.1;
    NSData * dd = UIImageJPEGRepresentation(image, compressionQuality);
    int maxFileSize = 550*1024;
    int i = 0;
    
    while ([dd length] > maxFileSize && compressionQuality > minCompression)
    {
        ++i;
        compressionQuality -= 0.1;
        dd = UIImageJPEGRepresentation(image, compressionQuality);
    }
    return dd;
}

+ (UIImage *)compressImageIfNeed:(UIImage *)image
{
    NSData * dd = UIImageJPEGRepresentation(image, 1.0);
    if (dd.length > 1000000) {
        image = [FImageProcessor scaledImageDataWithImage:image];
    }
    
    return image;
}

+ (UIImage *)scaledImageDataWithImage:(UIImage *)curImage
{
    CGSize size = curImage.size;
    CGFloat max = size.width > size.height ? size.width : size.height;
    if (max > 1000) {
        double ratio = 1000/max;
        curImage = [self scaledImageWithImage:curImage scaledToSize:(CGSize){size.width * ratio, size.height * ratio}];
    }
    return curImage;// UIImageJPEGRepresentation(curImage, 0.97);
}

+ (UIImage *)scaledImageWithImage:(UIImage *)curImage scaledToSize:(CGSize)newSize {
    double ratio;
    //make a new square size, that is the resized imaged width
    CGSize sz = CGSizeMake(newSize.width, newSize.height);
    
    if (curImage.size.width > curImage.size.height) {
        ratio = newSize.width / curImage.size.width;
        
    } else {
        ratio = newSize.height / curImage.size.height;
    }
    
    CGRect clipRect = CGRectMake(0, 0,
                                 (ratio * curImage.size.width),
                                 (ratio * curImage.size.height));
    
    //start a new context, with scale factor 0.0 so retina displays get
    //high quality image
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(sz, YES, 0.0);
    } else {
        UIGraphicsBeginImageContext(sz);
    }
    UIRectClip(clipRect);
    [curImage drawInRect:clipRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


+ (UIImage *)cropImage:(UIImage *)image withAspectRatio:(CGFloat)ratio
{
    float w = image.size.height * ratio;
    return [FImageProcessor image:image cropToRect:CGRectMake(0, 0, w, image.size.height)];
}

+ (UIImage *)image:(UIImage *)image cropToRect:(CGRect)rect
{
    CGImageRef croppedImage = CGImageCreateWithImageInRect(image.CGImage, rect);
    UIImage *newImage = [UIImage  imageWithCGImage:croppedImage scale:1.0 orientation:image.imageOrientation];
    CGImageRelease(croppedImage);
    
    return newImage;
}

+ (UIImage *)drawLines:(NSArray<NSArray<DrawPoint *> *> *)lines toImage:(UIImage *)image
{
    CGSize imageSize = image.size;
    UIGraphicsBeginImageContext(imageSize);
    [image drawInRect:CGRectMake(0, 0, imageSize.width, imageSize.height)];
    
    for (NSArray * line in lines) {
        for (int i = 0; i < line.count - 1; ++i) {
            DrawPoint * temp1 = line[i];
            DrawPoint * temp2 = line[i+1];
            CGContextMoveToPoint(UIGraphicsGetCurrentContext(), temp1.point.x, temp1.point.y);
            CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), temp2.point.x, temp2.point.y);
            CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
            CGContextSetLineWidth(UIGraphicsGetCurrentContext(), temp1.brush );
            CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), temp1.red, temp1.green, temp1.blue, 1.0);
            CGContextSetBlendMode(UIGraphicsGetCurrentContext(),kCGBlendModeNormal);
            CGContextStrokePath(UIGraphicsGetCurrentContext());
        }
        DrawPoint * last = line.lastObject;
        CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
        CGContextSetLineWidth(UIGraphicsGetCurrentContext(), last.brush);
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), last.red, last.green, last.blue, 1.0);
        CGContextMoveToPoint(UIGraphicsGetCurrentContext(), last.point.x, last.point.y);
        CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), last.point.x, last.point.y);
        CGContextStrokePath(UIGraphicsGetCurrentContext());
        CGContextFlush(UIGraphicsGetCurrentContext());
    }
    
    UIImage * result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return result;
}

@end
