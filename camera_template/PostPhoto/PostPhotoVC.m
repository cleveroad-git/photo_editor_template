//
//  PostPhotoVC.m
//  Frizby
//
//  Created by Gromov on 30.09.15.
//  Copyright © 2015 Gromov. All rights reserved.
//

#import "PostPhotoVC.h"
#import "FCommonStyles.h"
#import "FImageProcessor.h"

//#import "UIView + Animation.h"
#import "PPColorButton.h"
#import "DrawPoint.h"
#import "FitCurveNSO.h"

#define CBUTTON_MARGIN 8

 char get_line_intersection(float p0_x, float p0_y, float p1_x, float p1_y,
                                  float p2_x, float p2_y, float p3_x, float p3_y, float *i_x, float *i_y)
{
    float s1_x, s1_y, s2_x, s2_y;
    s1_x = p1_x - p0_x;     s1_y = p1_y - p0_y;
    s2_x = p3_x - p2_x;     s2_y = p3_y - p2_y;
    
    float s, t;
    s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
    t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);
    
    if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
    {
        // Collision detected
        if (i_x != NULL)
            *i_x = p0_x + (t * s1_x);
        if (i_y != NULL)
            *i_y = p0_y + (t * s1_y);
        return 1;
    }
    
    return 0; // No collision
}

char getLineIntersection(CGPoint p1, CGPoint p2, CGPoint p3, CGPoint p4)
{
    NSLog(@"getLineIntersection - %@,%@,%@,%@", NSStringFromCGPoint(p1), NSStringFromCGPoint(p2), NSStringFromCGPoint(p3), NSStringFromCGPoint(p4));
    return get_line_intersection(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y, p4.x, p4.y, NULL, NULL);
}

typedef NS_ENUM(NSInteger, DrawingState) {
    DrawingStateNone = 0,
    DrawingStateDrawing,
    DrawingStateErase,
    DrawingStateEraseLine
};

@interface PostPhotoVC () {
    IBOutlet UIButton * postButton;
    IBOutlet PPColorButton * colorButton;
    IBOutlet UIView * colorButtonsPane;
    IBOutlet UIView * tapView;
    IBOutlet UIImageView * imageView;
    IBOutlet UILabel * commentLabel;
    IBOutlet UITextField * hiddenTF;
    
    IBOutlet NSLayoutConstraint * visiblePaneConstraint;
    IBOutlet NSLayoutConstraint * hiddenPaneConstraint;
    IBOutlet NSLayoutConstraint * commentLabelYConstraint;
    NSArray * colorsArray;
    NSMutableArray * buttonsArray;
    CGPoint lastPoint;
    
    CGFloat red;
    CGFloat green;
    CGFloat blue;
    CGFloat brush;
    CGFloat opacity;
    NSMutableArray<DrawPoint *> * pointsArray;
    DrawingState drawingState;
    NSMutableArray<NSArray *> * linesArray;
}

@end
/////////////////////////////////////////////////////////////////////////////

@implementation PostPhotoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    colorsArray = [FCommonStyles getColors];
    [self createColorButtons];
 
    imageView.image = self.sourseImage;
    drawingState = DrawingStateDrawing;
    [self.view sendSubviewToBack:imageView];
    hiddenTF.autocapitalizationType = UITextAutocapitalizationTypeNone;
    hiddenTF.autocorrectionType = UITextAutocorrectionTypeNo;
    brush = 2.0;
    pointsArray = [NSMutableArray array];
    linesArray = [NSMutableArray array];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    CGRect imageRect = [FImageProcessor calculateClientRectOfImageInUIImageView:imageView];
    UIGraphicsBeginImageContext(imageRect.size);
    [imageView.image drawInRect:CGRectMake(0, 0, imageRect.size.width, imageRect.size.height)];
    _sourseImage = imageView.image = UIGraphicsGetImageFromCurrentImageContext();
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createColorButtons
{
    CGRect start = colorButton.frame;
    [colorButton makeCircleFormWithColor:[UIColor blackColor]];
    [colorButton setBorder];
    buttonsArray = [NSMutableArray arrayWithCapacity:11];
    [buttonsArray addObject:colorButton];
    for (int i = 1; i < 12; ++i) {
        float d = i == 10 || i == 11 ? 4.0 : 0.0;
        PPColorButton * cb = [[PPColorButton alloc] initWithFrame:(CGRect){CGPointMake(CGRectGetMinX(start) - d, CGRectGetMinY(start) + i*(CBUTTON_MARGIN + CGRectGetHeight(start))), CGSizeMake(start.size.width + d*2, start.size.height + d*2)}];
        [cb addTarget:self action:@selector(colorButtonClick:) forControlEvents:UIControlEventTouchUpInside];
//        cb.backgroundColor = colorsArray[i];
        cb.tag = 300 + i;
//        cb.layer.cornerRadius = CGRectGetHeight(cb.frame)/2.0;
//        cb.layer.masksToBounds = YES;
        [cb makeCircleFormWithColor:colorsArray[i]];
        
        if (i == 10) {
            [cb setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [cb setTitle:@"E" forState:UIControlStateNormal];
        }
        else if (i == 11) {
            [cb setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [cb setTitle:@"EL" forState:UIControlStateNormal];
        }
    
        [colorButtonsPane addSubview:cb];
        [buttonsArray addObject:cb];
    }
}

- (void)setSourseImage:(UIImage *)sourseImage
{
    UIImage * compressed = nil;// [FImageProcessor compressImageIfNeed:sourseImage];
    _sourseImage = compressed ? compressed : sourseImage;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark    Actions

- (IBAction)postButtonClick:(UIButton *)sender
{
    [self hideColorButtonsPane];
    UIImage * result = [FImageProcessor imageFromView:commentLabel];
    CGRect imageRect = [FImageProcessor calculateClientRectOfImageInUIImageView:imageView];
    float d = imageView.image.size.width / imageRect.size.width;
    CGRect convert = [self.view convertRect:commentLabel.frame toView:imageView];
    convert = (CGRect){{CGRectGetMinX(convert) - CGRectGetMinX(imageRect), CGRectGetMinY(convert) - CGRectGetMinY(imageRect)}, convert.size};
    CGRect new = [FImageProcessor multiplyRect:convert multiplier:d];
    result = [FImageProcessor drawImage:result intoImage:imageView.image inRect:new];
    imageView.image = result;
    [self test];
    
    NSData * imageData = [FImageProcessor getCompressedDataForImage:result];
}

- (void)popToFeedList
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)test
{
    commentLabel.hidden = YES;
    postButton.hidden = YES;
}

- (IBAction)colorButtonClick:(PPColorButton *)sender
{
    [buttonsArray makeObjectsPerformSelector:@selector(removeBorder)];
    [sender setBorder];
    if (sender.tag < 310) {
        commentLabel.textColor = colorsArray[sender.tag - 300];
        [commentLabel.textColor getRed:&red green:&green blue:&blue alpha:NULL];
    }

//    drawingState = sender.tag == 310 ? DrawingStateErase : DrawingStateDrawing;
    switch (sender.tag) {
        case 310:
            drawingState = DrawingStateErase;
            break;
            
        case 311:
            drawingState = DrawingStateEraseLine;
            break;
            
        default:
            drawingState = DrawingStateDrawing;
            break;
    }
}

- (IBAction)showKeyboardGesture:(UITapGestureRecognizer *)recognizer
{
    if (CGRectContainsPoint((CGRect){self.view.frame.origin, CGSizeMake(CGRectGetWidth(self.view.frame) - 70, CGRectGetHeight(self.view.frame))}, [recognizer locationInView:self.view])) {
        if ([hiddenTF isFirstResponder]) {
            [hiddenTF resignFirstResponder];
        }
        else {
            [hiddenTF becomeFirstResponder];
        }
    }
}

- (IBAction)scaleComment:(UIPinchGestureRecognizer *)recognizer
{
    UIFont * startFont = commentLabel.font;
    UIFont * newFont = [UIFont fontWithName:startFont.fontName size:startFont.pointSize*recognizer.scale];
    commentLabel.font = newFont;
    CGRect rect = commentLabel.frame;
//    float w = CGRectGetWidth(rect)*recognizer.scale;
    float h = CGRectGetHeight(rect)*recognizer.scale;
//    float x = CGRectGetMinX(rect) - (w - CGRectGetWidth(rect))/2;
//    float y = CGRectGetMinY(rect) - (h - CGRectGetHeight(rect))/2;
    commentLabelYConstraint.constant -= (h - CGRectGetHeight(rect))/2;
//    commentLabel.frame = CGRectMake(x, y, w, h);
    recognizer.scale = 1;
}

- (IBAction)dragComment:(UIPanGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        lastPoint = [recognizer locationInView:self.view];
    }
    else if (recognizer.state == UIGestureRecognizerStateChanged) {
        CGPoint pointMoved = [recognizer locationInView:self.view];
        float move = lastPoint.y - pointMoved.y;
        commentLabel.frame = (CGRect){CGPointMake(CGRectGetMinX(commentLabel.frame), CGRectGetMinY(commentLabel.frame) - move), commentLabel.frame.size};
        commentLabelYConstraint.constant -= move;
        lastPoint = pointMoved;
    }
    else if ((recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled)) {
        
    }
}

- (void)hideColorButtonsPane
{
    visiblePaneConstraint.priority = 250;
    hiddenPaneConstraint.priority = 999;
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)showColorButtonsPane
{
    visiblePaneConstraint.priority = 999;
    hiddenPaneConstraint.priority = 250;
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString * text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    commentLabel.text = text;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    lastPoint = [touch locationInView:imageView];
    CGRect imageRect = [FImageProcessor calculateClientRectOfImageInUIImageView:imageView];
    lastPoint = CGPointMake(lastPoint.x - CGRectGetMinX(imageRect), lastPoint.y - CGRectGetMinY(imageRect));
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint currentPoint = [touch locationInView:imageView];
    if (drawingState == DrawingStateDrawing)
    {
        CGRect imageRect = [FImageProcessor calculateClientRectOfImageInUIImageView:imageView];
        currentPoint = CGPointMake(currentPoint.x - CGRectGetMinX(imageRect), currentPoint.y - CGRectGetMinY(imageRect));
        UIGraphicsBeginImageContext(imageRect.size);
//        [imageView.image drawInRect:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [imageView.image drawInRect:CGRectMake(0, 0, imageRect.size.width, imageRect.size.height)];
        CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
        CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
        CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
        CGContextSetLineWidth(UIGraphicsGetCurrentContext(), brush );
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), red, green, blue, 1.0);
        CGContextSetBlendMode(UIGraphicsGetCurrentContext(),kCGBlendModeNormal);
        
        CGContextStrokePath(UIGraphicsGetCurrentContext());
        imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    //    [self.tempDrawImage setAlpha:opacity];
        UIGraphicsEndImageContext();
        lastPoint = currentPoint;
        [self addPoint:[self getDrawPointByPoint:lastPoint]];
    }
    else if (drawingState == DrawingStateErase)
    {
        CGRect imageRect = [FImageProcessor calculateClientRectOfImageInUIImageView:imageView];
        CGRect eraseRect = CGRectMake(currentPoint.x - 10 - CGRectGetMinX(imageRect), currentPoint.y - 10 - CGRectGetMinY(imageRect), 20, 20);
        [self eraseInRect:eraseRect];
    }
    else if (drawingState == DrawingStateEraseLine) {
        NSLog(@"currentPoint - %@", NSStringFromCGPoint(currentPoint));
        [self addPoint:[self getDrawPointByPoint:currentPoint]];
        if (pointsArray.count == 2) {
            [self checkIntersection];
            [pointsArray removeObject:pointsArray.firstObject];
//            NSLog(@"pointsArray - %@", pointsArray);
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    CGRect imageRect = [FImageProcessor calculateClientRectOfImageInUIImageView:imageView];
    UIGraphicsBeginImageContext(imageRect.size);
    //        [imageView.image drawInRect:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [imageView.image drawInRect:CGRectMake(0, 0, imageRect.size.width, imageRect.size.height)];
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), brush);
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), red, green, blue, opacity);
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    CGContextFlush(UIGraphicsGetCurrentContext());
    imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContext(imageRect.size);
    [imageView.image drawInRect:CGRectMake(0, 0, imageRect.size.width, imageRect.size.height) blendMode:kCGBlendModeNormal alpha:1.0];
    [imageView.image drawInRect:CGRectMake(0, 0, imageRect.size.width, imageRect.size.height) blendMode:kCGBlendModeNormal alpha:opacity];
    imageView.image = UIGraphicsGetImageFromCurrentImageContext();
//    self.tempDrawImage.image = nil;
    UIGraphicsEndImageContext();
    if (drawingState == DrawingStateDrawing || drawingState == DrawingStateEraseLine) {
        [self drowLines];
    }
}

- (void)checkIntersection
{
    for (int i = 0; i < linesArray.count; ++i) {
        NSArray<DrawPoint *> * line = linesArray[i];
        for (int i = 1; i < line.count; ++i) {
            if (getLineIntersection(pointsArray[0].point, pointsArray[1].point, line[i-1].point, line[i].point)) {
                [linesArray removeObject:line];
                break;
            }
        }
    }
}

- (void)addPoint:(DrawPoint *)dp
{
    if (pointsArray.count) {
        CGPoint prev = pointsArray.lastObject.point;
        if (CGPointEqualToPoint(prev, dp.point)) {
            return;
        }
    }
    [pointsArray addObject:dp];
}

- (void)eraseInRect:(CGRect)rect
{
    CGImageRef imageRef = CGImageCreateWithImageInRect([_sourseImage CGImage], rect);
    UIImage * eraseSpace = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    imageView.image = [FImageProcessor drawImage:eraseSpace intoImage:imageView.image inRect:rect];
}

- (DrawPoint *)getDrawPointByPoint:(CGPoint)point
{
    DrawPoint * newDP = [[DrawPoint alloc] init];
    newDP.point = point;
    newDP.red = red;
    newDP.green = green;
    newDP.blue = blue;
    newDP.brush = brush;
    
    return newDP;
}

/*
- (void)removeErasedPoint:(CGPoint)point
{
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (drawingState == DrawingStateNone) {
        return;
    }
    else if (drawingState == DrawingStateDrawing) {
        UITouch *touch = [touches anyObject];
        lastPoint = [touch locationInView:self.view];
        [self getDrawPointByPoint:lastPoint];
    }
    else {
        UITouch *touch = [touches anyObject];
        lastPoint = [touch locationInView:self.view];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (drawingState == DrawingStateNone) {
        return;
    }
    else if (drawingState == DrawingStateDrawing) {
        UITouch *touch = [touches anyObject];
        CGPoint currentPoint = [touch locationInView:self.view];
        [self getDrawPointByPoint:currentPoint];
    }
    else {
        
    }
//    if (pointsArray.count > 2) {
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            [self drowLine];
//        });
//    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (drawingState == DrawingStateNone) {
        return;
    }
        [self drowLine];
        
        UIGraphicsBeginImageContext(imageView.frame.size);
        [imageView.image drawInRect:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) blendMode:kCGBlendModeNormal alpha:1.0];
        [imageView.image drawInRect:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) blendMode:kCGBlendModeNormal alpha:opacity];
        imageView.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();

}
*/
- (void)drowLines
{
    FitCurveNSO * fc = [[FitCurveNSO alloc] init];
    if (pointsArray.count > 2) {
        NSMutableArray * res = [fc getFitCurveFromArray:pointsArray];
        [linesArray addObject:res];
    }
    imageView.image = [FImageProcessor drawLines:linesArray toImage:_sourseImage];
    
    [pointsArray removeAllObjects];
}

@end
