//
//  PostPhotoVC.h
//  Frizby
//
//  Created by Gromov on 30.09.15.
//  Copyright © 2015 Gromov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FFrizby;

@protocol PostPhotoDelegate <NSObject>

- (void)photoDidPosted:(FFrizby *)photo;

@end

@interface PostPhotoVC : UIViewController

@property (nonatomic, weak) id<PostPhotoDelegate> delegate;
@property (nonatomic, strong) UIImage * sourseImage;

@end
