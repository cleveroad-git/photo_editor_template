//
//  PPColorButton.m
//  frizby
//
//  Created by Gromov on 19.10.15.
//  Copyright © 2015 Gromov. All rights reserved.
//

#import "PPColorButton.h"
#import "FCommonStyles.h"

@implementation PPColorButton

- (void)makeCircleFormWithColor:(UIColor *)color
{
    self.layer.cornerRadius = self.frame.size.width/2;
    self.backgroundColor = color;
//    self.layer.borderWidth = 3.0;
//    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.clipsToBounds = YES;
    backgroundColor = color;
}

- (void)setBorder
{
    UIColor * borderColor = [FCommonStyles isRedColor:backgroundColor] ? [UIColor whiteColor] : [UIColor redColor];

    self.layer.borderWidth = 3.0;
    self.layer.borderColor = borderColor.CGColor;
}

- (void)removeBorder
{
    self.layer.borderWidth = 0.0;
}

@end
