//
//  DrawPoint.h
//  frizby
//
//  Created by Gromov on 27.11.15.
//  Copyright © 2015 Gromov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DrawPoint : NSObject

@property CGPoint point;
@property CGFloat red;
@property CGFloat green;
@property CGFloat blue;
@property CGFloat brush;

- (DrawPoint *)copyWithPoint:(CGPoint)point;

@end
