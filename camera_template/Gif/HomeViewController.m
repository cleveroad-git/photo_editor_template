//
//  HomeViewController.m
//  Mov2GIF
//
//  Created by Alexey Titov on 26.11.15.
//  Copyright © 2015 Alexey Titov. All rights reserved.
//

#import "HomeViewController.h"
#import "CameraManager.h"
#import "FileCell.h"
#import "GIFPreviewController.h"
#import "GIFGenerator.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface HomeViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray *files;
@property (nonatomic, strong) CameraManager *cameraManager;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *plusButton;
@property (weak, nonatomic) IBOutlet UIView *framesCountButtons;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *plusItem;
@property (weak, nonatomic) IBOutlet UILabel *noFilesLabel;

@property (nonatomic, strong) NSURL *fileURL;
@property (nonatomic, strong) NSURL *resultURL;
@property (nonatomic, strong) AVPlayerLayer *layer;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.framesCountButtons.hidden = YES;
    
    self.files = [self filesInDocumentsDirectory];
}

#pragma mark - IBActions

- (IBAction)addNewPressed:(id)sender {
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Camera is unavailable"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
        return;
    }
    
    self.cameraManager = [[CameraManager alloc] initWithParentController:self];
    [self.cameraManager setCameraType:CameraTypeVideo];
    [self.cameraManager showCamera];
}

- (IBAction)framesButtonPressed:(UIButton *)sender {
    
    int tag = (int)sender.tag;
    
    GIFGenerator *generator = [[GIFGenerator alloc] initWithSourceFileURL:self.fileURL frameCount:tag];
    self.resultURL = generator.createGIF;
    
    if (self.resultURL != nil)
    {
        NSData *data = [NSData dataWithContentsOfURL:self.resultURL];
        
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
        NSString *dateString = [dateFormatter stringFromDate:[NSDate new]];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = paths[0];
        NSString *filePath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.gif", dateString]];
        
//        [data writeToFile:filePath atomically:YES];
        NSError *error = nil;
        [data writeToFile:filePath options:NSDataWritingAtomic error:&error];
        
        [self.layer removeFromSuperlayer];
        self.tableView.hidden = NO;
//        self.plusButton.hidden = NO;
        self.plusItem.enabled = YES;
        self.framesCountButtons.hidden = YES;
        self.files = [self filesInDocumentsDirectory];
        [self.tableView reloadData];
        
        if (error == nil)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"GIF saved successfully to Camera Roll" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Preview", nil];
            [alert show];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Saving GIF to Documents failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
    else
    {
        [self.layer removeFromSuperlayer];
        self.tableView.hidden = NO;
//        self.plusButton.hidden = NO;
        self.plusItem.enabled = YES;
        self.framesCountButtons.hidden = YES;
        self.files = [self filesInDocumentsDirectory];
        [self.tableView reloadData];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Creating GIF failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
//    VideoPreviewController *controller = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"videoPreviewID"];
//    controller.url = [info valueForKey:UIImagePickerControllerMediaURL];
//    [self presentViewController:controller animated:NO completion:nil];
    
    self.tableView.hidden = YES;
//    self.plusButton.hidden = YES;
    self.plusItem.enabled = NO;
    self.framesCountButtons.hidden = NO;
    if (self.noFilesLabel.hidden == NO)
    {
        self.noFilesLabel.hidden = YES;
    }
    
    self.fileURL = [info valueForKey:UIImagePickerControllerMediaURL];
    AVPlayer *avPlayer = [AVPlayer playerWithURL:self.fileURL];
    
    self.layer = [AVPlayerLayer playerLayerWithPlayer:avPlayer];
    avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    self.layer.frame = CGRectMake((CGRectGetWidth(self.view.frame) - 100)/2, 100, 100, 150);
    [self.view.layer addSublayer: self.layer];
    
    [avPlayer play];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.files.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FileCell *cell = [tableView dequeueReusableCellWithIdentifier:@"fileCell" forIndexPath:indexPath];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = paths[0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:self.files[indexPath.row]];
    
    cell.fileImageView.image = [UIImage imageWithContentsOfFile:filePath];
    cell.fileNameLabel.text = self.files[indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 80;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = paths[0];
        NSString *filePath = [documentsPath stringByAppendingPathComponent:self.files[indexPath.row]];
        
        NSFileManager *manager = [NSFileManager defaultManager];
        NSError *error = nil;
        if ([manager removeItemAtPath:filePath error:&error])
        {
            NSLog(@"file successfully removed");
        }
        else
        {
            NSLog(@"removing file failed: %@", error);
        }
        
        self.files = [self filesInDocumentsDirectory];
        [self.tableView reloadData];
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsPath = paths[0];
//    NSString *filePath = [documentsPath stringByAppendingPathComponent:self.files[indexPath.row]];
//    
//    GIFPreviewController *controller = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"gifPreviewID"];
//    controller.url = [NSURL fileURLWithPath:filePath];
//    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1)
    {
//        GIFPreviewController *controller = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"gifPreviewID"];
//        controller.url = self.resultURL;
//        NSLog(@"result url: %@", self.resultURL);
//        [self presentViewController:controller animated:YES completion:nil];
        [self performSegueWithIdentifier:@"toPreviewController" sender:self];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"toPreviewController"])
    {
        GIFPreviewController *destination = segue.destinationViewController;
        destination.url = self.resultURL;
    }
    else if ([segue.identifier isEqualToString:@"toPreviewFromCell"])
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = paths[0];
        NSString *filePath = [documentsPath stringByAppendingPathComponent:self.files[[self.tableView indexPathForSelectedRow].row]];
        
        GIFPreviewController *destination = segue.destinationViewController;
        destination.url = [NSURL fileURLWithPath:filePath];
    }
}

#pragma mark - Helpers

- (NSArray *)filesInDocumentsDirectory {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = paths[0];
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsPath error:NULL];
    
    for (NSString *file in directoryContent)
    {
        NSLog(@"file: %@", file);
    }
    
    if ([directoryContent count] == 0)
    {
        self.tableView.hidden = YES;
        self.noFilesLabel.hidden = NO;
    }
    else
    {
        self.tableView.hidden = NO;
        self.noFilesLabel.hidden = YES;
    }
    
    return directoryContent;
}

@end
