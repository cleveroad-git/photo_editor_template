//
//  GIFPreviewController.h
//  MovToFifTest
//
//  Created by Alexey Titov on 26.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GIFPreviewController : UIViewController

@property (nonatomic, strong) NSURL *url;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
