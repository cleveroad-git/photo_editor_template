//
//  GIFPreviewController.m
//  MovToFifTest
//
//  Created by Alexey Titov on 26.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "GIFPreviewController.h"
#import "UIImage+GIF.h"

@implementation GIFPreviewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc]
                                    initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                    target:self
                                    action:@selector(shareAction:)];
    self.navigationItem.rightBarButtonItem = shareButton;
    
    NSData *gifData = [NSData dataWithContentsOfURL:self.url];
    self.imageView.image = [UIImage sd_animatedGIFWithData:gifData];
}

- (IBAction)backPressed:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)shareAction:(UIButton *)sender {
    NSData *gifData = [NSData dataWithContentsOfURL:self.url];
    NSArray *activityItems = @[[UIImage imageWithData:gifData]];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
    [self presentViewController:activityVC animated:YES completion:nil];
}

@end
