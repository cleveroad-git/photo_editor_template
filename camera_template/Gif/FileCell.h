//
//  fileCell.h
//  Mov2GIF
//
//  Created by Alexey Titov on 26.11.15.
//  Copyright © 2015 Alexey Titov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FileCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *fileImageView;
@property (weak, nonatomic) IBOutlet UILabel *fileNameLabel;

@end
