//
//  GIFGenerator.h
//  MovToFifTest
//
//  Created by Alexey Titov on 26.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <ImageIO/ImageIO.h>
#import <AVFoundation/AVFoundation.h>

@interface GIFGenerator : NSObject

- (id)initWithSourceFileURL:(NSURL *)sourceFileURL frameCount:(NSInteger)frameCount;
- (NSURL *)createGIF;

@end
