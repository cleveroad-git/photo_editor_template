//
//  GIFGenerator.m
//  MovToFifTest
//
//  Created by Alexey Titov on 26.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "GIFGenerator.h"

@interface GIFGenerator ()

@property (nonatomic, strong) NSURL *sourceFileURL;
@property (nonatomic, assign) NSInteger frameCount;

@end

@implementation GIFGenerator

- (id)initWithSourceFileURL:(NSURL *)sourceFileURL frameCount:(NSInteger)frameCount {
    
    self = [super init];
    if (self)
    {
        _sourceFileURL = sourceFileURL;
        _frameCount = frameCount;
    }
    return self;
}

- (NSURL *)createGIF {
    
    int32_t timeIntervar = 600;
    Float64 toleranceSeconds = 0.01;
    int loopCount = 0;
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:self.sourceFileURL options:nil];
    
    CGFloat movieLength = (CGFloat)asset.duration.value / (CGFloat)asset.duration.timescale;
    CGFloat increment = movieLength / self.frameCount;
    
    NSString *temporaryFile = [NSTemporaryDirectory() stringByAppendingPathComponent:@"file_name.gif"];
    NSURL *fileURL = [NSURL fileURLWithPath:temporaryFile];
    
    CGImageDestinationRef destination = CGImageDestinationCreateWithURL((CFURLRef)fileURL, kUTTypeGIF, self.frameCount, nil);
    if (destination == nil)
    {
        NSLog(@"creating destination failed");
    }
    
    const void *tkeys[1] = { kCGImagePropertyGIFLoopCount };
    const void *tvalues[1] = { CFNumberCreate(0, kCFNumberSInt32Type, &loopCount) };
    CFDictionaryRef tfileProperties = CFDictionaryCreate(NULL, tkeys, tvalues, 1, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
    
    const void *keys[1] = { kCGImagePropertyGIFDictionary };
    const void *values[1] = { tfileProperties };
    CFDictionaryRef fileProperties = CFDictionaryCreate(NULL, keys, values, 1, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
    
    NSLog(@"file properties: %@", fileProperties);
    
    CGImageDestinationSetProperties(destination, fileProperties);
    
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform = YES;
    
    CMTime tolerance = CMTimeMakeWithSeconds(toleranceSeconds, timeIntervar);
    generator.requestedTimeToleranceBefore = tolerance;
    generator.requestedTimeToleranceAfter = tolerance;
    
    for (int frameNumber = 0; frameNumber < self.frameCount; frameNumber++)
    {
        CGFloat seconds = increment * frameNumber;
        CMTime time = CMTimeMakeWithSeconds(seconds, timeIntervar);
        
        NSError *error = nil;
        CGImageRef imageRef = [generator copyCGImageAtTime:time actualTime:nil error:&error];
        if (error == nil)
        {
            CGImageDestinationAddImage(destination, imageRef, nil);
        }
        else
        {
            NSLog(@"adding frame to destination failed: %@", error);
        }
    }
    
    CGImageDestinationSetProperties(destination, fileProperties);
    
    if (!CGImageDestinationFinalize(destination))
    {
        NSLog(@"destination finalize error");
    }
    
    return fileURL;
}

@end
