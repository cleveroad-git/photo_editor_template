//
//  main.m
//  camera_template
//
//  Created by Gromov on 22.12.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
