//
//  CREditVideoVC.h
//  camera_template
//
//  Created by Gromov on 11.02.16.
//  Copyright © 2016 cleveroad. All rights reserved.
//

//https://developer.apple.com/library/ios/samplecode/AVSimpleEditoriOS/Introduction/Intro.html#//apple_ref/doc/uid/DTS40012797

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreMedia/CoreMedia.h>
#import <QuartzCore/QuartzCore.h>

#import "AVSECommand.h"
#import "AVSETrimCommand.h"
#import "AVSEExportCommand.h"

@interface CREditVideoVC : UIViewController
{
    AVSEExportCommand * exportCommand;
}

@property (strong) NSString * videoURL;

@property AVPlayer * player;
@property AVPlayerLayer * playerLayer;
@property double currentTime;
@property (readonly) double duration;

@property AVMutableComposition * composition;
@property AVMutableVideoComposition * videoComposition;
@property AVMutableAudioMix * audioMix;
@property AVAsset * inputAsset;
@property AVAsset * resultAsset;
@property CALayer * watermarkLayer;

@property IBOutlet UIActivityIndicatorView * loadingSpinner;
@property IBOutlet UILabel * unplayableLabel;
@property IBOutlet UILabel * noVideoLabel;
@property IBOutlet UILabel * protectedVideoLabel;

@property IBOutlet UIButton * playPauseButton;
@property IBOutlet UIButton * exportButton;
@property IBOutlet UIView * playerView;
@property IBOutlet UIProgressView * exportProgressView;

@property IBOutlet UIView *sliderView;

- (void)reloadPlayerView;
- (void)exportWillBegin;
- (void)exportDidEnd;
- (void)editCommandCompletionNotificationReceiver:(NSNotification*)notification;
- (void)exportCommandCompletionNotificationReceiver:(NSNotification*)notification;

- (IBAction)playPauseToggle:(id)sender;
- (IBAction)edit:(id)sender;
- (IBAction)exportToMovie:(id)sender;

- (void)resetAsset:(AVAsset *)inputAsset;

@end
