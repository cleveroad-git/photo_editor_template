//
//  DetailViewController.h
//  camera_template
//
//  Created by Gromov on 22.12.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

