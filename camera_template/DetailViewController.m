//
//  DetailViewController.m
//  camera_template
//
//  Created by Gromov on 22.12.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "DetailViewController.h"
#import "CameraManager.h"
#import "PostPhotoVC.h"

@interface DetailViewController () {
    CameraManager * cameraManager;
}

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
            
        // Update the view.
        [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
        self.detailDescriptionLabel.text = [self.detailItem description];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showcamera
{
    cameraManager = [[CameraManager alloc] initWithParentController:self];
    [cameraManager showCamera];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    PostPhotoVC * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PostPhotoVC"];
    vc.sourseImage = image;
//    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:NULL];
}


@end
