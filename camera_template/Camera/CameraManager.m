//
//  CameraManager.m
//  SENDER
//
//  Created by Nick Gromov on 11/14/14.
//  Copyright (c) 2014 PrivatBank. All rights reserved.
//

#import "CameraManager.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "K9ImagePickerController.h"
#import <AVFoundation/AVFoundation.h>
#import "PECropView.h"


@interface CameraManager () {
    __weak UIViewController<UIImagePickerControllerDelegate> * parentController;
    __weak K9ImagePickerController * imagePickerController;
    CameraType cameraType;
    BOOL isRecording;
    NSURL * localUrl;
    UIImage * image;
    K9ImagePickerController * picker;
    CGRect cameraOverlayViewFrame;
    NSTimer * trackerTimer;
    
    IBOutlet PECropView * peCropView;
    __strong IBOutlet UIView * overlayCameraView;
    __strong IBOutlet UIView * usePhotoCameraView;
    __strong IBOutlet UIButton * backButton;
    IBOutlet UIButton * cancelButton;
    IBOutlet UIButton * changeCameraButton;
    IBOutlet UIButton * changeTypeButton;
    IBOutlet UIButton * startButton;
    IBOutlet UIButton * libraryButton;
    IBOutlet UIButton * flashModeButton;
    IBOutlet UILabel * timeLabel;
    IBOutlet NSLayoutConstraint * bottomViewHeight;
}
//// overlayCameraView buttons
- (IBAction)cancelButtonClick:(id)sender;
- (IBAction)changeCameraButtonClick:(id)sender;
- (IBAction)changeTypeButtonClick:(id)sender;
- (IBAction)startButtonClick:(id)sender;
- (IBAction)libraryButtonClick:(id)sender;
- (IBAction)changeFlashModeButtonClick:(id)sender;

//// usePhotoCameraView buttons
- (IBAction)usePhotoButtonClick:(id)sender;
- (IBAction)backButtonClick:(id)sender;

@end

@implementation CameraManager

static CGFloat DegreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;};

- (id)initWithParentController:(UIViewController<UIImagePickerControllerDelegate> *)controller
{
    if (self = [super init]) {
        parentController = controller;
        cameraType = CameraTypeImage;
        isRecording = NO;
    }
    return self;
}

- (void)dealloc
{
    imagePickerController = nil;
    parentController = nil;
}

- (void)setCameraType:(CameraType)type
{
    cameraType = type;
}

- (void)showCamera
{
    picker = [[K9ImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.allowsEditing = NO;
    picker.delegate = self;
    picker.showsCameraControls = NO;
//    picker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *) kUTTypeImage, nil];
    
    cameraType = cameraType == CameraTypeImage ? CameraTypeVideo : CameraTypeImage;
    [self changeTypeButtonClick:nil];

    
    [[NSBundle mainBundle] loadNibNamed:@"OverlayCameraView" owner:self options:nil];
    overlayCameraView.frame = picker.cameraOverlayView.frame;
    picker.cameraOverlayView = overlayCameraView;
  
    float yOffset = 60.0;
    CGAffineTransform translate = CGAffineTransformMakeTranslation(0.0, yOffset);
    picker.cameraViewTransform = CGAffineTransformScale(translate, 1, 1);
    overlayCameraView = nil;
    [parentController presentViewController:picker animated:YES completion:NULL];
    imagePickerController = picker;
    imagePickerController.navigationBar.tintColor = [UIColor redColor];
//    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [[NSBundle mainBundle] loadNibNamed:@"UsePhotoOCView" owner:self options:nil];
//    usePhotoCameraView.frame = picker.cameraOverlayView.frame;
    [self performSelector:@selector(setPhotoCameraViewFrame) withObject:nil afterDelay:0.3];
}

- (void)setPhotoCameraViewFrame
{
    cameraOverlayViewFrame = picker.cameraOverlayView.frame;
    usePhotoCameraView.frame = picker.cameraOverlayView.frame;
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [parentController setNeedsStatusBarAppearanceUpdate];
}

- (IBAction)cancelButtonClick:(id)sender
{
    [parentController.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)changeCameraButtonClick:(id)sender
{
    if (picker.cameraDevice == UIImagePickerControllerCameraDeviceRear) {
        picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
    }
    else {
        picker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    }
}

- (IBAction)changeTypeButtonClick:(id)sender
{
    if (cameraType == CameraTypeImage) {
        cameraType = CameraTypeVideo;
        picker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *) kUTTypeMovie, nil];
        
        [startButton setBackgroundImage:[UIImage imageNamed:@"start_recording.png"] forState:UIControlStateNormal];
        [changeTypeButton setBackgroundImage:[UIImage imageNamed:@"media_photo.png"] forState:UIControlStateNormal];
    }
    else {
        cameraType = CameraTypeImage;
        picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
        
        [startButton setBackgroundImage:[UIImage imageNamed:@"button_take_photo.png"] forState:UIControlStateNormal];
        [changeTypeButton setBackgroundImage:[UIImage imageNamed:@"media_camera.png"] forState:UIControlStateNormal];
        [flashModeButton setHidden:[UIImagePickerController isFlashAvailableForCameraDevice:imagePickerController.cameraDevice]];
    }
}

- (IBAction)startButtonClick:(id)sender
{
    if (cameraType == CameraTypeImage) {
        [imagePickerController takePicture];
    }
    else {
        if (!isRecording) {
            isRecording = [imagePickerController startVideoCapture];
            timeLabel.text = @"00";
            [self startTracker];
        }
        else {
            [imagePickerController stopVideoCapture];
            [self stopTracker];
        }
    }
}

#pragma mark -
#pragma mark NSTimer

- (void)startTracker
{
    if (!trackerTimer) {
        trackerTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                        target:self
                                                      selector:@selector(trackerTimerDidFired:)
                                                      userInfo:nil
                                                       repeats:YES];
        [trackerTimer fire];
    }
}

- (void)stopTracker
{
    if (trackerTimer) {
        [trackerTimer invalidate];
        trackerTimer = nil;
    }
}

- (void)trackerTimerDidFired:(NSTimer *)timer
{
    NSUInteger number = [timeLabel.text integerValue];
    timeLabel.text = [NSString stringWithFormat:@"%2lu", ++number];
    if (number == 30) {
        [self startButtonClick:nil];
    }
}

- (IBAction)libraryButtonClick:(id)sender
{
    if (imagePickerController.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else {
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    }
}

- (IBAction)changeFlashModeButtonClick:(id)sender
{
    UIButton * button = (UIButton *)sender;
    if ([UIImagePickerController isFlashAvailableForCameraDevice:imagePickerController.cameraDevice]) {
        if (imagePickerController.cameraFlashMode == UIImagePickerControllerCameraFlashModeAuto) {
            imagePickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeOff;
            //            [button setBackgroundImage:[UIImage imageNamed:@"flash_disabled.png"] forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:@"icFlashD.png"] forState:UIControlStateNormal];
        }
        else {
            imagePickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
            //            [button setBackgroundImage:[UIImage imageNamed:@"flash_auto.png"] forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:@"icPhR.png"] forState:UIControlStateNormal];
        }
        [button setHidden:NO];
    }
    else {
        [button setHidden:YES];
    }
}

- (IBAction)usePhotoButtonClick:(id)sender
{
//    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@""
//                                                          message:@"Вы хотите отправить эту фотографию?"
//                                                         delegate:self
//                                                cancelButtonTitle:@"Отмена"
//                                                otherButtonTitles: @"Ок", nil];
//    [myAlertView show];
    NSDictionary * info = @{UIImagePickerControllerOriginalImage:peCropView.image, UIImagePickerControllerEditedImage:peCropView.croppedImage};
    [parentController imagePickerController:imagePickerController didFinishPickingMediaWithInfo:info];
    [parentController.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        NSDictionary * info = @{UIImagePickerControllerOriginalImage:peCropView.image, UIImagePickerControllerEditedImage:peCropView.croppedImage};
        [parentController imagePickerController:imagePickerController didFinishPickingMediaWithInfo:info];
        [parentController.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self backButtonClick:self];
    }
}

- (IBAction)backButtonClick:(id)sender
{
    if (imagePickerController.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [usePhotoCameraView removeFromSuperview];
    }
    [[NSBundle mainBundle] loadNibNamed:@"OverlayCameraView" owner:self options:nil];
    overlayCameraView.frame = imagePickerController.cameraOverlayView.frame;
    imagePickerController.cameraOverlayView = overlayCameraView;
}

- (IBAction)rotateButtonClick:(id)sender
{
    [peCropView setRotationAngle:peCropView.rotationAngle + M_PI_2 ];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"imagePickerController - %@",info);
    
    NSString * type = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([type isEqualToString:(NSString *)kUTTypeVideo] || [type isEqualToString:(NSString *)kUTTypeMovie])
    {
        [parentController imagePickerController:imagePickerController didFinishPickingMediaWithInfo:info];
        [parentController.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        localUrl = (NSURL *)[info valueForKey:UIImagePickerControllerReferenceURL];
        image = (UIImage*)[info objectForKey:@"UIImagePickerControllerOriginalImage"];
        
        if (localUrl) {
            [[NSBundle mainBundle] loadNibNamed:@"UsePhotoOCView" owner:self options:nil];
            peCropView.image = image;
            usePhotoCameraView.frame = cameraOverlayViewFrame;
            [imagePickerController.view addSubview:usePhotoCameraView];
        }
        else {
            [[NSBundle mainBundle] loadNibNamed:@"UsePhotoOCView" owner:self options:nil];
            usePhotoCameraView.frame = picker.cameraOverlayView.frame;
            peCropView.image = image;
            imagePickerController.cameraOverlayView = usePhotoCameraView;
        }
//        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    }
}

- (UIImage *)squareImageWithImage:(UIImage *)curImage scaledToSize:(CGSize)newSize {
    double ratio;
    //make a new square size, that is the resized imaged width
    CGSize sz = CGSizeMake(newSize.width, newSize.height);
    
    if (curImage.size.width > curImage.size.height) {
        ratio = newSize.width / curImage.size.width;
        
    } else {
        ratio = newSize.height / curImage.size.height;
    }
    
    CGRect clipRect = CGRectMake(0, 0,
                                 (ratio * curImage.size.width),
                                 (ratio * curImage.size.height));
    
    //start a new context, with scale factor 0.0 so retina displays get
    //high quality image
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(sz, YES, 0.0);
    } else {
        UIGraphicsBeginImageContext(sz);
    }
    UIRectClip(clipRect);
    [curImage drawInRect:clipRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
