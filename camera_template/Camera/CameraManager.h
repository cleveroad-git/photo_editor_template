//
//  CameraManager.h
//
//
//  Created by Nick Gromov on 11/14/15.
//  Copyright (c) 2014 Cleveroad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, CameraType) {
    CameraTypeImage,
    CameraTypeVideo
};


@interface CameraManager : NSObject <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

- (id)initWithParentController:(UIViewController<UIImagePickerControllerDelegate> *)controller;
- (void)showCamera;
- (void)setCameraType:(CameraType)type;

@end
